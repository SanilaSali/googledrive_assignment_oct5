package com.test.controllers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.apache.commons.io.FileUtils;
import org.docx4j.XmlUtils;
import org.docx4j.convert.in.xhtml.XHTMLImporterImpl;
import org.docx4j.jaxb.Context;
import org.docx4j.model.structure.PageSizePaper;
import org.docx4j.openpackaging.contenttype.ContentType;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.exceptions.InvalidFormatException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.WordprocessingML.AltChunkType;
import org.docx4j.openpackaging.parts.WordprocessingML.AlternativeFormatInputPart;
import org.docx4j.openpackaging.parts.WordprocessingML.NumberingDefinitionsPart;
import org.docx4j.relationships.Relationship;
import org.docx4j.wml.CTAltChunk;

public class HtmlToDocConverter {

    /**
     * @param args
     * @throws FileNotFoundException
     * @throws JAXBException
     * @throws Docx4JException
     */
    public static void main(String[] args) throws FileNotFoundException,
            JAXBException, Docx4JException {
        // TODO Auto-generated method stub

    	System.out.println("HtmlToDocConverter - START");
    			
		/*String xhtml= "<div align=\"center\">" +
								"Hello" +
				"</div>";  
		
		//String html = "<div><h1>TESTING H1</h1>	<h2>TESTING H2</h2>	<h3>TESTING H3</h3></div>";
    	  String xhtml = "<html>" +
                  "<head>" +
                  "    <style type=\"text/css\">" +
                  "    h2 {" +
                  "        text-align: center;" +
                  "        font-family: Times New Roman;" +
                  "        font-size: 11 pt;" +
                  "    }" +
                  "    </style>" +
                  "</head>" +
                  "<body>" +
                  "    <h2> Line on the first page</h2>" +
                  "    <h2> Line on the second page</h2>" +
                  "</body>" +
                  "</html>";
    	
		WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.createPackage();
		
        XHTMLImporterImpl XHTMLImporter = new XHTMLImporterImpl(wordMLPackage);

		wordMLPackage.getMainDocumentPart().getContent().addAll( 
				XHTMLImporter.convert( xhtml, null) );
	
	System.out.println(
			XmlUtils.marshaltoString(wordMLPackage.getMainDocumentPart().getJaxbElement(), true, true));
	
	String outputfilepath = "D:/Sanila/CrackVerbal/HTMLOut.docx";
      
	wordMLPackage.save(new java.io.File("D:/Sanila/CrackVerbal/HTMLOut.docx") );
	
	 System.out.println("HtmlToDocConverter - END");*/
    	String outputfilepath = "D:/Sanila/CrackVerbal/HTMLOut.docx";

        try {
        	System.out.println("HtmlToDocConverter - START");
        	 String html = "<html><head><title>Import me</title></head><body><p><h1>TESTING H1</h1>	<h2>TESTING H2</h2>	<h3>TESTING H3</h3></body></html>";

        	 WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.createPackage(PageSizePaper.A4, true);

             XHTMLImporterImpl XHTMLImporterForContent = new XHTMLImporterImpl(wordMLPackage);
             wordMLPackage.getMainDocumentPart().addAltChunk(AltChunkType.Html, html.getBytes());
             wordMLPackage.save(new File(outputfilepath));
             System.out.println("HtmlToDocConverter - END");

        }

        catch (Docx4JException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e){}

    
	
  }
        

}