package com.test.controllers;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBElement;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.converter.WordToHtmlConverter;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xwpf.converter.core.FileURIResolver;
import org.apache.poi.xwpf.converter.xhtml.XHTMLConverter;
import org.apache.poi.xwpf.converter.xhtml.XHTMLOptions;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.docx4j.Docx4J;
import org.docx4j.Docx4jProperties;
import org.docx4j.TextUtils;
import org.docx4j.XmlUtils;
import org.docx4j.convert.in.xhtml.XHTMLImporterImpl;
import org.docx4j.convert.out.HTMLSettings;
import org.docx4j.convert.out.html.AbstractHtmlExporter;
import org.docx4j.convert.out.html.HtmlExporterNG2;
import org.docx4j.jaxb.Context;
import org.docx4j.model.structure.PageSizePaper;
import org.docx4j.openpackaging.contenttype.ContentType;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.PartName;
import org.docx4j.openpackaging.parts.WordprocessingML.AltChunkType;
import org.docx4j.openpackaging.parts.WordprocessingML.AlternativeFormatInputPart;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.docx4j.openpackaging.parts.WordprocessingML.NumberingDefinitionsPart;
import org.docx4j.relationships.Relationship;
import org.docx4j.wml.Body;
import org.docx4j.wml.CTAltChunk;
import org.docx4j.wml.ContentAccessor;
import org.docx4j.wml.Text;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.AbstractInputStreamContent;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.InputStreamContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files;
import com.google.api.services.drive.Drive.Files.Update;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.Revision;
import com.google.api.services.drive.model.RevisionList;
import com.test.model.VersionFiles;
import com.test.model.VersionFilesImpl;



@Controller
public class DocumentController {
	
	 private static final String APPLICATION_NAME = "Google Drive API Java TestApp";
	 private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	 private static final java.io.File CREDENTIALS_FOLDER = new java.io.File(System.getProperty("user.home"), "credentials");
	 private static final String CLIENT_SECRET_FILE_NAME = "client_secret.json";
	 private static final List<String> SCOPES = Collections.singletonList(DriveScopes.DRIVE);
	 private static NetHttpTransport HTTP_TRANSPORT;
	 private static Drive service;
	 private static File myFolder;
	 
	 @Autowired
	 VersionFilesImpl versionService;
	 
		
	@RequestMapping(value="/home", method=RequestMethod.GET)
	public ModelAndView viewHomePage(HttpServletRequest request, HttpServletResponse response){
		
		ModelAndView mav = new ModelAndView("home");
		
		try{
			googleDriveConnect();
		    HttpSession httpsession = request.getSession();
            String docToOpen = request.getParameter("param1");
            String revisionToOpen = request.getParameter("param2");
          
           	List<VersionFiles> versionHistory = versionService.getVersionHistory();
    		
    		//OPEN DOCUMENT
    		if(null!=revisionToOpen){
    			String docContent = viewDocument(docToOpen,revisionToOpen);
    			httpsession.setAttribute("docContent", docContent);
    			httpsession.setAttribute("versionToOpen", revisionToOpen);
    			httpsession.setAttribute("docToOpen",docToOpen);
    		}
    		//VIEW HOME PAGE
    		else{
    				httpsession.setAttribute("docContent", "");
    				httpsession.setAttribute("versionToOpen", "");
    				httpsession.setAttribute("docToOpen", "");
    		}
    		request.setAttribute("versionHistory",versionHistory);
    				
		}catch(Exception e)	{}
		//mav.addObject("home", new Document());
		return mav;
		
 }
	
	@RequestMapping(value="/home", method=RequestMethod.POST)
	public ModelAndView uploadDocument(HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		googleDriveConnect();
		ModelAndView mav = null;
		java.io.File uploadFile = null;
		java.io.File uploadFileTemp = null;
		VersionFiles versionfile = new VersionFiles();
		HttpSession httpsession = request.getSession();
		
		String versionToOpen = request.getParameter("param1");
		String content = request.getParameter("mydoc");
		String docName = null;
		
		if(null!=request.getParameter("reset")){
			httpsession.setAttribute("docContent", "");
			httpsession.setAttribute("docToOpen", "");
		}if(null!=request.getParameter("save")){
						
			if(null!=content && !content.equals("")){
				String versionType = "Original";
				FileOutputStream out =null;
				docName = request.getParameter("docName");
				if(null == docName || docName.equals("")){
		    	  docName = (String) httpsession.getAttribute("docToOpen");
		    	  versionType = "Current";
				}
				if(!(docName.endsWith(".docx"))){
		    		  System.out.println(" No .docx");
		    		  docName = docName+".docx";
				}
				System.out.println(" =========> Document Content to upload :"+content);
				System.out.println(" =========>Uploading Document "+docName+"........");
			
				String fileName = "D:\\Sanila\\CrackVerbal\\"+docName;
				String fileNametemp = "D:\\Sanila\\CrackVerbal\\Temp"+docName;
				
				// ONLINE EDITOR CODE - START
				/*WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.createPackage();
				AlternativeFormatInputPart afiPart = new AlternativeFormatInputPart(new PartName("/hw.html"));
	            afiPart.setBinaryData(content.getBytes());
	            afiPart.setContentType(new ContentType("text/html"));
	            Relationship altChunkRel = wordMLPackage.getMainDocumentPart().addTargetPart(afiPart);
	             
	            // .. the bit in document body
	            CTAltChunk ac = Context.getWmlObjectFactory().createCTAltChunk();
	            ac.setId(altChunkRel.getId() );
	            wordMLPackage.getMainDocumentPart().addObject(ac);
	                      
	            wordMLPackage.getContentTypeManager().addDefaultContentType("html", "text/html");
	            
	            System.out.println(" =========>Converted HTML to DOC format:");
	            uploadFileTemp = new java.io.File(fileNametemp);
	            
	            
	            
	            wordMLPackage.save(uploadFileTemp);
	            System.out.println(" =========> TEMP File CREATED..........");
	            MainDocumentPart documentPart = wordMLPackage.getMainDocumentPart(); 
	            org.docx4j.wml.Document wmlDocumentEl = (org.docx4j.wml.Document)documentPart.getJaxbElement();
                StringWriter str = new StringWriter();
                org.docx4j.TextUtils.extractText(wmlDocumentEl, str);
                String strString = str.toString();
                System.out.println(" =========> TEMP File Content "+ strString);
	            //ONLINE EDITOR CODE - END*/
				 WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.createPackage(PageSizePaper.A4, true);

	             XHTMLImporterImpl XHTMLImporterForContent = new XHTMLImporterImpl(wordMLPackage);
	             wordMLPackage.getMainDocumentPart().addAltChunk(AltChunkType.Html, content.getBytes());
	             wordMLPackage.save(new java.io.File(fileNametemp));
				
				uploadFile = new java.io.File(fileNametemp);
				
				
			
				//Create folder Documento in Google Drive if not exists
				String documentoId = getFolderId("Documento");
				System.out.println("Folder Documento Id is "+documentoId);
			
				if(null == documentoId){
					File newFolder = new File();
					newFolder.setName("Documento");
					newFolder.setMimeType("application/vnd.google-apps.folder");
					myFolder = service.files().create(newFolder).setFields("id").execute();
					System.out.println("Created Folder Documento...");
					System.out.println("Folder Documento Id is "+myFolder.getId());
					documentoId = myFolder.getId();
				}
			
				Revision googleFile = createGoogleFile(documentoId, "application/rtf", docName, uploadFile);
				
				if (uploadFile.exists()) {
					uploadFile.delete();
   			    	//System.out.println(uploadFile.getName() + " is NOT deleted!");
       	        } else {
       	            System.out.println("Delete operation is failed.");
       	        }
				 
				versionfile.setVersion(versionType);
				versionfile.setRevision_id(googleFile.getId());
				versionfile.setDocument(docName);
				versionfile.setCreatedDate(Calendar.getInstance().getTime());
				versionService.saveVersionHistory(versionfile);
			}else{
				httpsession.setAttribute("message", "Please enter Document name and Document content");
			}
		}
		List<VersionFiles> versionHistory = versionService.getVersionHistory();
		request.setAttribute("versionHistory",versionHistory);
		httpsession.setAttribute("docContent", "");
		httpsession.setAttribute("versionToOpen", "");
		httpsession.setAttribute("docToOpen", "");
		
		mav = new ModelAndView("home");
	    return mav;
	}
	
	public void googleDriveConnect()throws Exception{

		// 1: Create CREDENTIALS_FOLDER
    	if (!CREDENTIALS_FOLDER.exists()) {
    		 CREDENTIALS_FOLDER.mkdirs();
        	}
    	     
    	HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
    	
       //Read client_secret.json file & create Credential object.
        Credential credential = getCredentials(HTTP_TRANSPORT);
 
        //Create Google Drive Service.
        service = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential) //
                .setApplicationName(APPLICATION_NAME).build();
        
        
 
   }

	public Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
	    java.io.File clientSecretFilePath = new java.io.File(CREDENTIALS_FOLDER, CLIENT_SECRET_FILE_NAME);
	    if (!clientSecretFilePath.exists()) {
	    	throw new FileNotFoundException("Please copy " + CLIENT_SECRET_FILE_NAME //
                + " to folder: " + CREDENTIALS_FOLDER.getAbsolutePath());
	    }

	    // Load client secrets.
	    InputStream in = new FileInputStream(clientSecretFilePath);
	    GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

	    // Build flow and trigger user authorization request.
	    GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
            clientSecrets, SCOPES).setDataStoreFactory(new FileDataStoreFactory(CREDENTIALS_FOLDER))
                    .setAccessType("offline").build();

	    return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
	}
	
	 // Create Google File from java.io.File
    public static Revision createGoogleFile(String googleFolderIdParent, String contentType, String customFileName, java.io.File uploadFile) throws IOException {
         AbstractInputStreamContent uploadStreamContent = new FileContent(contentType, uploadFile);
         return _createGoogleFile(googleFolderIdParent, contentType, customFileName, uploadStreamContent);
    }
    
    private static Revision _createGoogleFile(String googleFolderIdParent, String contentType, String customFileName, AbstractInputStreamContent uploadStreamContent) throws IOException {
    	File fileMetadata = new File();
        File file = new File();
        //Drive.Files.Create createRequest;
        
        String fileId = checkIfFileExistsInFolder(customFileName);
        Revision revision = null;
        if(null == fileId){
        	fileMetadata.setName(customFileName);
            List<String> parents = Arrays.asList(googleFolderIdParent);
            fileMetadata.setParents(parents);
            System.out.println("===========> IN _createGoogleFile -> uploadStreamContent :"+uploadStreamContent);
        	file = service.files().create(fileMetadata, uploadStreamContent).execute();
        	String newFileId = file.getId();
        	try {
                RevisionList revisions = service.revisions().list(newFileId).execute();
                List<Revision> revisionList = revisions.getRevisions();

                for(Revision newRevision : revisionList) {
                    revision = service.revisions().get(newFileId, newRevision.getId()).execute();
                    revision.setPublished(true);
                  
                }
             } catch (IOException e) {
                 System.out.println("An error occured: " + e);
             }
         }
        else{
        	//Changes
        	com.google.api.services.drive.model.File newContent = new File();
        	//newContent.setTrashed(true);
        	//List<String> parents = Arrays.asList(googleFolderIdParent);
        	//newContent.setParents(parents);
        	//service.files().update(fileId, newContent).execute();
        	//Changes -END
        	//file = service.files().update(fileId, newContent, uploadStreamContent).execute();
        	file = service.files().update(fileId, newContent, uploadStreamContent).setAddParents(googleFolderIdParent).setFields("id, parents").execute();
        	try {
        		
                RevisionList revisions = service.revisions().list(fileId).execute();
                List<Revision> revisionList = revisions.getRevisions();

                for(Revision newRevision : revisionList) {
                    revision = service.revisions().get(fileId, newRevision.getId()).execute();
                    revision.setPublished(true);
                   
                }
             } catch (IOException e) {
                 System.out.println("An error occured: " + e);
             }
        }
        return revision;
    }
    
    private static String getFolderId(String foldername) throws IOException{
    	String fileId = null;
    	FileList result = service.files().list().setPageSize(10).setFields("nextPageToken, files(id, name)").execute();
        List<File> files = result.getFiles();
        if (files == null || files.isEmpty()) {
            System.out.println("No files found.");
        } else {
           for (File file : files) {
        	   //System.out.println(" File Name :"+file.getName());
                if(file.getName().equals(foldername)){
                	 	fileId = file.getId();
                }
            }  
        }
        return fileId;
    }
    
    private static String checkIfFileExistsInFolder(String docname) throws IOException{
    	String fileId = null;
    	List<File> result = new ArrayList<File>();
        Files.List request = null;
    	//FileList result = service.files().list().setPageSize(10).setFields("nextPageToken, files(id, name)").execute();
    	try {
    		String documentoId = getFolderId("Documento");
            request = service.files().list();//plz replace your FOLDER ID in below linez
            FileList files = service.files().list().setQ("'"+documentoId+"' in parents").execute();
            result.addAll(files.getFiles());          
            request.setPageToken(files.getNextPageToken());
            for(File f:result){
            	if(docname.equals(f.getName())){
            		fileId = f.getId();
                }
             }
          } 
        catch (IOException e)
        {
          System.out.println("An error occurred: " + e);
          request.setPageToken(null);
        }
        return fileId;
    }
    
    
    private String viewDocument(String docname, String revisionToOpen) {
		// TODO Auto-generated method stub
			
		String words = "";
		FileOutputStream openedFile;
		FileInputStream fis;
		java.io.File  f = null;
		Revision revision = null;
		
		if(null!=revisionToOpen){
			try{
				//FileList result = service.files().list().setPageSize(10).setFields("nextPageToken, files(id, name)").execute();
				
				String documentoId = getFolderId("Documento");
				System.out.println("********* IN  viewDocument ********** documentoId is "+documentoId);
				        	
		        //String fileIdToOpen = checkIfFileExists(docname);
		        String fileIdToOpen = checkIfFileExistsInFolder(docname);
		        
		        if(null!=fileIdToOpen && !fileIdToOpen.equals("")){
	        		
		        RevisionList revisions = service.revisions().list(fileIdToOpen).execute();
		        List<Revision> revisionList = revisions.getRevisions();

		        	for(Revision newRevision : revisionList) {
	                 
		        		revision = service.revisions().get(fileIdToOpen, newRevision.getId()).execute();
		        		String revisionId = revision.getId();
		        		System.out.println("==============> Revision ID: " +revisionId );
	                 	        			
		        		if(null!= revisionId && !revisionId.equals("") && revisionId.equals(revisionToOpen)){

			        		String filename ="D:\\Sanila\\CrackVerbal\\FromRepo.docx";
			        		f = new java.io.File(filename); 
			        		openedFile =new FileOutputStream(f);
			        		//System.out.println("==============> GOING TO CONVERT FROM RTF TO DOC" );
		        			service.revisions().get(fileIdToOpen, revisionToOpen).executeMediaAndDownloadTo(openedFile);
		        			//service.files().export(fileIdToOpen, "application/pdf").executeMediaAndDownloadTo(openedFile);
		        			//System.out.println("==============> CONVERTED AND READ..............." );
		        			System.out.println("==============> Read file from Google Drive" );
		        			System.out.println("==============> f.exists() = "+f.exists() );
		        			System.out.println("==============> f.length() = "+f.length() );
		        			System.out.println("DOC FILE TO CONVERT TO HTML = "+filename);
		        			words = parseDocxToHTML(filename);
		        			System.out.println("HTML OUTPUT = "+words);	
		           			}
		        	}
			}
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
		
	
	return words;

	}
 
	public String parseDocxToHTML(String inputFilePath) {
		WordprocessingMLPackage wmlPackage = null;
		HTMLSettings settings = Docx4J.createHTMLSettings();
		java.io.File newHtmlFile = null;
		StringBuilder redefined = null;
		try {
		
		//String fileName = home.getName();
		String ext = FilenameUtils.getExtension(inputFilePath);
		System.out.println("---------> ext.equals(.docx)" +ext.equals("docx"));
		
		if (ext.equals("docx")) {
		wmlPackage = Docx4J.load(new java.io.File(inputFilePath));
		settings.setImageDirPath(inputFilePath + "_files");
		settings.setImageTargetUri(inputFilePath.substring(inputFilePath.lastIndexOf("/") + 1) + "_files");
		settings.setWmlPackage(wmlPackage);
		System.out.println("******************* DocToHtmlConverter -1 ");
		String val = "html, body, div, span, h1, h2, h3, h4, h5, h6, p, a, img, ol, ul, li, table, caption, tbody, tfoot, thead, tr, th, td "
		+ "{ margin: 0; padding: 0; border: 0;}" + "body {line-height: 1;} ";
		;
		settings.setUserCSS(val);

		OutputStream outputStream = new FileOutputStream(new java.io.File("D://Sanila//CrackVerbal//output.html"));
		//OutputStream outputStream = new ByteArrayOutputStream();
		Docx4jProperties.setProperty("docx4j.Convert.Out.HTML.OutputMethodXML",true);
		Docx4J.toHTML(settings, outputStream, Docx4J.FLAG_EXPORT_PREFER_XSL);
		//System.out.println(((ByteArrayOutputStream)outputStream).toString()); 
		
		String input = FileUtils.readFileToString(new java.io.File("D://Sanila//CrackVerbal//output.html"),"UTF-8");
		org.jsoup.nodes.Document doc = Jsoup.parse(input);
		Elements elements = doc.body().children();
		redefined = new StringBuilder();
		String attributeValue = null;
		int index = 1;
		for (org.jsoup.nodes.Element element : elements) {
			if ((!element.tagName().equals("span")) &&( element.tagName().equals("div")
					|| element.tagName().equals("p") || element.tagName().equals("li")
					|| element.tagName().equals("table") || element.tagName().equals("td")
					|| element.tagName().equals("h1") || element.tagName().equals("h2")
					|| element.tagName().equals("h2") || element.tagName().equals("h3")
					|| element.tagName().equals("h4") || element.tagName().equals("h5")
					|| element.tagName().equals("h6"))) {
						attributeValue = "" + index;
						element.attr("id", attributeValue);
						index++;
						Elements childElement = element.children().select("*");
						for (org.jsoup.nodes.Element children : childElement) {
							if ((!children.tagName().equals("span")) &&(children.tagName().equals("div")
							|| children.tagName().equals("p") || children.tagName().equals("li")
							|| children.tagName().equals("table") || children.tagName().equals("td")
							|| children.tagName().equals("h1") || children.tagName().equals("h2")
							|| children.tagName().equals("h2") || children.tagName().equals("h3")
							|| children.tagName().equals("h4") || children.tagName().equals("h5")
							|| children.tagName().equals("h6"))) {
								attributeValue = "" + index;
								children.attr("id", attributeValue);
								index++;
							}
						}
						redefined.append(element.toString());
					}
			}
		newHtmlFile = new java.io.File("D://Sanila//CrackVerbal//RepoOutput.html");
		FileUtils.writeStringToFile(newHtmlFile, redefined.toString());
		//System.out.println("Output file is : " + newHtmlFile);
		newHtmlFile.delete();
		}
		
		} catch (Docx4JException | IOException e) {
		e.printStackTrace();
		}
		return redefined.toString();
		}
}