package com.test.controllers;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;








import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamResult;






import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.docx4j.Docx4J;
import org.docx4j.XmlUtils;
import org.docx4j.convert.out.HTMLSettings;
import org.docx4j.convert.out.flatOpcXml.FlatOpcXmlCreator;
import org.docx4j.convert.out.html.AbstractHtmlExporter;
import org.docx4j.convert.out.html.AbstractHtmlExporter.HtmlSettings;
import org.docx4j.convert.out.html.HtmlExporterNG2;
import org.docx4j.jaxb.Context;
import org.docx4j.jaxb.NamespacePrefixMapperUtils;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.xml.sax.SAXException;
import org.apache.poi.xwpf.converter.core.FileURIResolver;
import org.apache.poi.xwpf.converter.xhtml.XHTMLConverter;
import org.apache.poi.xwpf.converter.xhtml.XHTMLOptions;

import com.lowagie.text.Document;
import com.lowagie.text.Element;

public class DocToHtmlConverter {
 
	public static void main(String args[]) throws IOException, SAXException, ParserConfigurationException {
		System.out.println("******************* DocToHtmlConverter ****************");
		DocToHtmlConverter docx = new DocToHtmlConverter();
		String inputFilePath = "D://Sanila//CrackVerbal//Test.docx";
		 String inputFilePath2 = "D:/Sanila/CrackVerbal/HTMLOut.docx";
		System.out.println("Input file is : " + inputFilePath);
		docx.parseDocxToHTML(inputFilePath);
		}

		public File parseDocxToHTML(String inputFilePath) {
		WordprocessingMLPackage wmlPackage = null;
		HTMLSettings settings = Docx4J.createHTMLSettings();
		File newHtmlFile = null;

		try {
		
			
		//String fileName = home.getName();
		String ext = FilenameUtils.getExtension(inputFilePath);
		System.out.println("---------> ext.equals(.docx)" +ext.equals("docx"));
		if (ext.equals("docx")) {
		wmlPackage = Docx4J.load(new File(inputFilePath));
		settings.setImageDirPath(inputFilePath + "_files");
		settings.setImageTargetUri(inputFilePath.substring(inputFilePath.lastIndexOf("/") + 1) + "_files");
		settings.setWmlPackage(wmlPackage);
		System.out.println("******************* DocToHtmlConverter -1 ");
		String val = "html, body, div, span, h1, h2, h3, h4, h5, h6, p, a, img, ol, ul, li, table, caption, tbody, tfoot, thead, tr, th, td "
		+ "{ margin: 0; padding: 0; border: 0;}" + "body {line-height: 1;} ";
		;
		settings.setUserCSS(val);

		OutputStream outputStream = new FileOutputStream(new File("D://Sanila//CrackVerbal//output.html"));

		Docx4J.toHTML(settings, outputStream, Docx4J.FLAG_EXPORT_PREFER_XSL);
		System.out.println("******************* DocToHtmlConverter -2 ");
		String input = FileUtils.readFileToString(new File("D://Sanila//CrackVerbal//output.html"),"UTF-8");
		org.jsoup.nodes.Document doc = Jsoup.parse(input);
		Elements elements = doc.body().children();
		StringBuilder redefined = new StringBuilder();
		String attributeValue = null;
		int index = 1;
		System.out.println("******************* DocToHtmlConverter -3 ");
		for (org.jsoup.nodes.Element element : elements) {
			System.out.println("******************* DocToHtmlConverter - after 3 element.tagName()"+element.tagName());	
			if ((!element.tagName().equals("span")) &&( element.tagName().equals("div")
					|| element.tagName().equals("p") || element.tagName().equals("li")
					|| element.tagName().equals("table") || element.tagName().equals("td")
					|| element.tagName().equals("h1") || element.tagName().equals("h2")
					|| element.tagName().equals("h2") || element.tagName().equals("h3")
					|| element.tagName().equals("h4") || element.tagName().equals("h5")
					|| element.tagName().equals("h6"))) {
						System.out.println("******************* DocToHtmlConverter -4 ");
						attributeValue = "" + index;
						System.out.println("******************* DocToHtmlConverter -4 attributeValue "+attributeValue);
						element.attr("id", attributeValue);
						index++;
						Elements childElement = element.children();
						System.out.println("******************* DocToHtmlConverter -4 childElement "+childElement.toString());
						for (org.jsoup.nodes.Element children : childElement) {
							System.out.println("******************* DocToHtmlConverter - after 3 children.tagName()"+children.tagName());	
							System.out.println("******************* DocToHtmlConverter -5 ");
							if ((!children.tagName().equals("span")) &&(children.tagName().equals("div")
							|| children.tagName().equals("p") || children.tagName().equals("li")
							|| children.tagName().equals("table") || children.tagName().equals("td")
							|| children.tagName().equals("h1") || children.tagName().equals("h2")
							|| children.tagName().equals("h2") || children.tagName().equals("h3")
							|| children.tagName().equals("h4") || children.tagName().equals("h5")
							|| children.tagName().equals("h6"))) {
								attributeValue = "" + index;
								children.attr("id", attributeValue);
								index++;
							}
						}
						redefined.append(element.toString());
						System.out.println("******************* DocToHtmlConverter -6 ");
					}
			}
		System.out.println("******************* DocToHtmlConverter -7 ");
		newHtmlFile = new File("D://Sanila//CrackVerbal//RepoOutput.html");
		System.out.println("Output file created");
		FileUtils.writeStringToFile(newHtmlFile, redefined.toString());
		System.out.println("Output file path is : " + newHtmlFile);
		System.out.println("******************* DocToHtmlConverter -8 ");
		System.out.println("******************* redefined.toString() " +redefined.toString());
		}
		
		} catch (Docx4JException | IOException e) {
		e.printStackTrace();
		}
		return newHtmlFile;
		}
}
